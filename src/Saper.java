import java.util.HashMap;

public class Saper {
    public static void main(String[] args) {

        HashMap<String, Integer> numberOfLines = returnHashMapWithCharsAndNumbers(26);
        System.out.println(numberOfLines);


    }

    static HashMap<String, Integer> returnHashMapWithCharsAndNumbers(int sizeOfGameField) {
        HashMap<String, Integer> hashMapWithNumbersAndChars = new HashMap<>();

        char[] charsForLines = new char[sizeOfGameField];
        char alphabet = 'A';
        int positionInAlphabet = 0;
        do {

            charsForLines[positionInAlphabet] = alphabet;
            hashMapWithNumbersAndChars.put(String.valueOf(charsForLines[positionInAlphabet]), positionInAlphabet + 2);
            alphabet++;
            positionInAlphabet++;
        } while (positionInAlphabet != sizeOfGameField);


        return hashMapWithNumbersAndChars;
    }

}