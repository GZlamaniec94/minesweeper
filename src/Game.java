
import data.GameField;
import data.Player;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {





        String playerName = getNameFromPlayer();
        Player player = new Player();
        player.setName(playerName);

        int sizeOfGameField = setGameFieldSize();
        GameField gameField = new GameField(sizeOfGameField);

        gameField.setGameField(gameField);







       player.checkFieldMove(gameField);




    }


    private static String getNameFromPlayer(){
        Scanner userInput = new Scanner(System.in);

        System.out.print("Please enter your name: ");
        String playerName = userInput.next();
        System.out.println("Hello " + playerName + ", have fun!");

        return playerName;
    }

    private static int setGameFieldSize () {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter size of game field, for example, enter '10' and it will be 10x10. Maximum is 26x26: ");
        String userInputSizeOfGame = userInput.next();

        int sizeOfGameField =Integer.parseInt(userInputSizeOfGame);

        while (true) {
            if (sizeOfGameField > 9 && sizeOfGameField < 27) {
                break;
            } else {
                System.out.println("Please enter correct size. It must be minimum 10 and maximum 26");
                sizeOfGameField = userInput.nextInt();
            }
        }
        return sizeOfGameField;
    }

    public static void printGameField(String[][] gameField) {

        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField.length; j++) {
               if (gameField[i][j].equals("*")){
                   System.out.format("%-4s", gameField[i][j].replace("*", " "));

               } else {
                   if (i > 2 || i < gameField.length - 4) {

                       System.out.format("%-4s", gameField[i][j]);

                   }
               }

            }
            System.out.println();

        }


    }



}
