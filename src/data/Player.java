package data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Player {


    private String name;

    public Player() {
    }


    @Override
    public String toString() {
        return "data.Player{" +
                "name='" + name + '\'' +
                '}';
    }


    public void checkFieldMove(GameField gameField) {


        String[][] emptyGameField = gameField.getGameFieldWithoutBombs(gameField);
        String[][] readyGameField = gameField.getGameField();


        printGameField(emptyGameField);
        printGameField(readyGameField);

        getNumberOfLineAndColumnFromPlayer(gameField);


    }


    private void getNumberOfLineAndColumnFromPlayer(GameField gameField) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter field: ");
        String numberOfLineAndColumn = input.next();



        boolean isUserInputCorrect = isUserInputCorrect(gameField, numberOfLineAndColumn);
        System.out.println(isUserInputCorrect);

    }

    private boolean isUserInputCorrect(GameField gameField, String userInput) {
        HashMap<String, Integer> charForLinesAndNumberOfLines = gameField.returnHashMapWithCharsAndNumbers(gameField);

        List<String> charsOfLinesList = new ArrayList<>(charForLinesAndNumberOfLines.keySet());
        List<String> numbersOfColumnList = new ArrayList<>();
        for (String key : charForLinesAndNumberOfLines.keySet()) {
            numbersOfColumnList.add(String.valueOf(charForLinesAndNumberOfLines.get(key)));
        }

        if (!(userInput.length() == 2)) {
            return false;
        } else {
            String firstCharOfUserInput = String.valueOf(userInput.charAt(0));
            String secondCharOfUserInput = String.valueOf(userInput.charAt(1));

            return (charsOfLinesList.contains(firstCharOfUserInput) || charsOfLinesList.contains(secondCharOfUserInput))
                    && (numbersOfColumnList.contains(firstCharOfUserInput) || numbersOfColumnList.contains(secondCharOfUserInput));

        }

    }


    public void setName(String name) {
        this.name = name;
    }

    private static void printGameField(String[][] gameField) {

        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField.length; j++) {
                if (gameField[i][j].equals("*")) {
                    gameField[i][j] = " ";
                    System.out.format("%-4s", gameField[i][j]);
                    gameField[i][j] = "*";
                } else {
                    if (i > 2 || i < gameField.length - 4) {

                        System.out.format("%-4s", gameField[i][j]);

                    }
                }

            }
            System.out.println();

        }


    }
}
