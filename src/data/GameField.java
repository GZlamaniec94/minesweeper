package data;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class GameField {

    private String[][] gameField;


    private int sizeOfGameField;


    public GameField(int sizeOfGameField) {
        this.sizeOfGameField = sizeOfGameField;
    }


    public void setGameField(GameField gameField) {

        String[][] readyGameField;
        readyGameField = gameField.getGameFieldWithoutBombs(gameField);
        gameField.placeBombsOnGameField(readyGameField, gameField);
        gameField.countingOfBombsPerCell(readyGameField);
        this.gameField = readyGameField;

    }

    private void countingOfBombsPerCell(String[][] gameFieldWithBombs) {

        for (int i = 2; i < gameFieldWithBombs.length - 2; i++) {

            for (int j = 2; j < gameFieldWithBombs.length - 2; j++) {
                int numberOfBombs = 0;
                if (gameFieldWithBombs[i][j].equals("b")) {
                    continue;
                } else {
                    for (int k = i - 1; k < i + 2; k++) {
                        for (int l = j - 1; l < j + 2; l++) {

                            if (gameFieldWithBombs[k][l].equals("b")) {
                                numberOfBombs += 1;
                            }
                        }

                    }
                }
                if (!(numberOfBombs == 0)) {
                    gameFieldWithBombs[i][j] = Integer.toString(numberOfBombs);
                }

            }


        }

    }

    private void placeBombsOnGameField(String[][] emptyGameField, GameField gameField) {
        int sizeOfGameField = gameField.getSizeOfGameField();
        int numberOfBombs = (sizeOfGameField * sizeOfGameField) / 12;
        for (int i = 0; i < numberOfBombs + 1; i++) {
            int line = ThreadLocalRandom.current().nextInt(2, sizeOfGameField + 2);
            int column = ThreadLocalRandom.current().nextInt(2, sizeOfGameField + 2);
            if (!emptyGameField[line][column].equals("")) {
                emptyGameField[line][column] = "b";
            } else {
                i = i - 1;
            }
        }


    }

    public String[][] getGameFieldWithoutBombs(GameField gameField) {

        String[][] gameFieldWithOutBombs = getEmptyGameField(gameField);
        HashMap<String, Integer> alphabetAndNumbers = returnHashMapWithCharsAndNumbers(gameField);
        for (int i = 2; i < gameFieldWithOutBombs.length - 2; i++) {
            int j = i - 1;
            String s = Integer.toString(j);
            if (i < 11) {
                gameFieldWithOutBombs[0][i] = "|" + s + "|";
            } else {
                gameFieldWithOutBombs[0][i] = "|" + s + "|";
            }

            if (i < 11) {
                gameFieldWithOutBombs[1][i] = "---";
            } else {
                gameFieldWithOutBombs[1][i] = "---";
            }
            if (i < 11) {
                gameFieldWithOutBombs[gameFieldWithOutBombs.length - 2][i] = "----";

            } else {
                gameFieldWithOutBombs[gameFieldWithOutBombs.length - 2][i] = "----";
            }
            gameFieldWithOutBombs[i][0] = String.valueOf(alphabetAndNumbers.keySet().toArray()[i - 2]);
            gameFieldWithOutBombs[i][1] = "|";
            gameFieldWithOutBombs[i][gameFieldWithOutBombs.length - 2] = "|";
        }
        return gameFieldWithOutBombs;
    }

    private String[][] getEmptyGameField(GameField gameField) {
        int sizeOfGameField = gameField.getSizeOfGameField();
        String[][] emptyGameField = new String[sizeOfGameField + 4][sizeOfGameField + 4];

        for (int i = 0; i < emptyGameField.length; i++) {
            for (int j = 0; j < emptyGameField.length; j++) {
                emptyGameField[i][j] = "*";
            }
        }
        return emptyGameField;
    }

    public HashMap<String, Integer> returnHashMapWithCharsAndNumbers(GameField gameField) {
        int sizeOfGameField = gameField.getSizeOfGameField();
        HashMap<String, Integer> hashMapWithNumbersAndChars = new HashMap<>();

        char[] charsForLines = new char[sizeOfGameField];
        char alphabet = 'A';
        int positionInAlphabet = 0;
        do {

            charsForLines[positionInAlphabet] = alphabet;
            hashMapWithNumbersAndChars.put(String.valueOf(charsForLines[positionInAlphabet]), positionInAlphabet + 2);
            alphabet++;
            positionInAlphabet++;
        } while (positionInAlphabet != sizeOfGameField);


        return hashMapWithNumbersAndChars;
    }


    public int getSizeOfGameField() {
        return sizeOfGameField;
    }


    public String[][] getGameField() {
        return gameField;
    }

    public void printGameField() {
        String[][] gameField = getGameField();
        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField.length; j++) {

                if (i > 2 || i < gameField.length - 4) {

                    System.out.format("%-4s", gameField[i][j]);
                }
            }
            System.out.println();

        }


    }

}
